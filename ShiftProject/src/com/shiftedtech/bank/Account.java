package com.shiftedtech.bank;

/**
 * Created by PaxoStudent on 4/18/2017.
 */
public class Account {
    private String accountNumber;
    private String firstName;
    private String lastName;
    private double balance;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        if(accountNumber.length()>=6) {
            this.accountNumber = accountNumber;
        }else {
            System.out.println("please provide valid account number which is 6 character");
        }
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount){

        balance=balance+amount;
    }

    public void withdraw(double amount){
        if(amount<=balance) {
            balance = balance - amount;
        }else {
            System.out.println("you can not withdraw amount :"+amount);
            System.out.println("you do not have enough money");
        }
    }
}
