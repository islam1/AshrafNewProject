package com.shiftedtech.bank;

/**
 * Created by PaxoStudent on 4/18/2017.
 */
public class MyBank {
    public static void main(String[] args) {
        System.out.println("This is object orinted programming.");

        Account myAccount= new Account();
        myAccount.setAccountNumber("123");
        myAccount.setFirstName("korim");
        myAccount.setLastName("islam");

       // myAccount.deposit(100);//you can deposit like this or
         //myAccount.balance=1000;  //You can deposit like this .
        System.out.println("my balance :"+myAccount.getBalance());

        myAccount.withdraw(40);
        myAccount.withdraw(60);
        myAccount.withdraw(60);
        System.out.println("my balance :"+myAccount.getBalance());
        myAccount.deposit(1000);
        System.out.println("my balance :"+myAccount.getBalance());
    }
}
