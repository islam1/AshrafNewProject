package com.shifted.qa.Script;

import com.shifted.qa.Base.BaseClasss;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by PaxoStudent on 5/11/2017.
 */
public class Button extends BaseClasss {

    @Test
    public void buttonClick() {
        WebElement button = driver.findElement(By.id("html-button-01"));
        button.click();
        WebElement text = driver.findElement(By.id("alert-btn-clicked"));
        Assert.assertTrue(text.getText().contains("Button is clicked"));
        text.click();
    }
}