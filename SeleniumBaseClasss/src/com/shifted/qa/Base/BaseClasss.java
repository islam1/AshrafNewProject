package com.shifted.qa.Base;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

/**
 * Created by PaxoStudent on 5/11/2017.
 */
public class BaseClasss {

    protected WebDriver driver = null;

    @Before
    public void setUp() {
        String os = System.getProperty("os.name").toLowerCase();
        System.out.println("Os Name: " + os);
        if (os.contains("mac")) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/driver/chromedriver");
        } else {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/driver/chromedriver.exe");
        }
        System.out.println("Driver Path:" + System.getProperty("webdriver.chrome.driver"));


//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("start-fullscreen");
//        driver = new ChromeDriver(options);
        driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        /*
        Test web Application URL http://shifttest.shiftedtech.com
         */
        driver.navigate().to("http://shifttest.shiftedtech.com/components/button");
    }

    @After
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}