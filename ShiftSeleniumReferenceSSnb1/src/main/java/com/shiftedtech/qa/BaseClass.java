package com.shiftedtech.qa;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by PaxoStudent on 5/11/2017.
 */
public class BaseClass {
    protected WebDriver driver = null;
    @Before
    public void SetUp(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/driver/chromedriver.exe");
        System.out.println("Driver Path:" + System.getProperty("webdriver.chrome.driver"));
        driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
       // driver.navigate().to("http://heatclinic.shiftedtech.com");
        driver.navigate().to("http://spree.shiftedtech.com");

       /* select chormedriver.exe , copy path . pest inside  system.getProperty"  here pest"
"user.dir")+"/driver/chromedriver.exe"
 */

    }
    @After
    public void tearDown(){
       // driver.close();
       // driver.quit();
    }
}
